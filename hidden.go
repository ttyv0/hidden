package main

import (
	"bufio"
	"io"
	"os"
	"sort"
	"strings"
)

func main() {
	size := len(os.Args) - 1
	if size <= 1 {
		return
	}
	pathName := os.Args[size] + "/"
	hiddenName := pathName + ".hidden"
	var hiddenFile *os.File
	defer hiddenFile.Close()
	cutArgs := make([]string, size-1)
	for i, s := range os.Args[1:size] {
		cutArgs[i] = strings.TrimPrefix(s, pathName)
	}
	sort.Strings(cutArgs)
	if _, err := os.Stat(hiddenName); os.IsNotExist(err) {
		hiddenFile, err = os.Create(hiddenName)
		if err != nil {
			return
		}
		writeStrings(hiddenFile, cutArgs)
	} else {
		hiddenFile, err = os.Open(hiddenName)
		if err != nil {
			return
		}
		bufferedReader := bufio.NewReader(hiddenFile)
		tempArgs := make([]string, 0)
		fileArgs := make([]string, 0)
		eof := false
		for !eof {
			line, err := bufferedReader.ReadString('\n')
			if err == io.EOF {
				err = nil
				eof = true
			}
			if !eof {
				line = strings.TrimSuffix(line, "\n")
				n := sort.SearchStrings(cutArgs, line)
				if n != len(cutArgs) && cutArgs[n] == line {
					tempArgs = cutArgs[:n]
					cutArgs = append(tempArgs, cutArgs[n+1:]...)
				} else {
					fileArgs = append(fileArgs, line)
				}
			}
		}
		fileArgs = append(fileArgs, cutArgs...)
		sort.Strings(fileArgs)
		hiddenFile.Close()
		hiddenFile, _ = os.Create(hiddenName)
		defer hiddenFile.Close()
		writeStrings(hiddenFile, fileArgs)
	}
}

func writeStrings(f *os.File, s []string) {
	for _, str := range s {
		if !strings.HasPrefix(str, ".") {
			f.WriteString(str + "\n")
		}
	}
}
